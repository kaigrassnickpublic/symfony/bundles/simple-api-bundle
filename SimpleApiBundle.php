<?php declare(strict_types=1);

namespace KaiGrassnick\SimpleApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SimpleApiBundle
 *
 * @package KaiGrassnick\SimpleApiBundle
 */
class SimpleApiBundle extends Bundle
{

}
