<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use KaiGrassnick\SimpleApiBundle\DTO\PaginatedItemsDTO;
use KaiGrassnick\SimpleApiBundle\DTO\Response\CollectionResponseDTO;
use KaiGrassnick\SimpleApiBundle\DTO\Response\PaginatedCollectionResponseDTO;
use KaiGrassnick\SimpleApiBundle\DTO\Response\SimpleResponseDTO;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractBaseController
 *
 * @package KaiGrassnick\SimpleApiBundle\Controller
 */
abstract class AbstractBaseController extends AbstractFOSRestController
{
    /**
     * @param PaginatedItemsDTO $paginatedItemsDTO
     *
     * @return Response
     */
    protected function customizablePaginatedCollectionResponse(PaginatedItemsDTO $paginatedItemsDTO): Response
    {
        return $this->paginatedCollectionHelper($paginatedItemsDTO, true);
    }


    /**
     * @param PaginatedItemsDTO $paginatedItemsDTO
     *
     * @return Response
     */
    protected function paginatedCollectionResponse(PaginatedItemsDTO $paginatedItemsDTO): Response
    {
        return $this->paginatedCollectionHelper($paginatedItemsDTO, false);
    }


    /**
     * @param array $result
     *
     * @return Response
     */
    protected function customizableCollectionResponse(array $result): Response
    {
        return $this->collectionResponseHelper($result, true);
    }


    /**
     * @param array $result
     *
     * @return Response
     */
    protected function collectionResponse(array $result): Response
    {
        return $this->collectionResponseHelper($result, false);
    }


    /**
     * @param mixed $result
     *
     * @return Response
     */
    protected function customizableSimpleResponse($result): Response
    {
        return $this->simpleResponseHelper($result, true);
    }


    /**
     * @param mixed $result
     *
     * @return Response
     */
    protected function simpleResponse($result): Response
    {
        return $this->simpleResponseHelper($result, false);
    }


    /**
     * @param PaginatedItemsDTO $paginatedItemsDTO
     * @param bool              $customizable
     *
     * @return Response
     */
    private function paginatedCollectionHelper(PaginatedItemsDTO $paginatedItemsDTO, bool $customizable): Response
    {
        $paginator = $paginatedItemsDTO->getPaginator();
        $items     = $paginatedItemsDTO->getItems();

        $limit          = $paginator->getQuery()->getMaxResults();
        $totalItemCount = $paginator->count();
        $start          = $paginator->getQuery()->getFirstResult();

        $response = (new PaginatedCollectionResponseDTO($items, $start, $totalItemCount, $limit, $customizable));

        return $this->handleView(View::create($response));
    }


    /**
     * @param array $result
     * @param bool  $customizable
     *
     * @return Response
     */
    private function collectionResponseHelper(array $result, bool $customizable): Response
    {
        $response = (new CollectionResponseDTO($result, $customizable));

        return $this->handleView(View::create($response));
    }


    /**
     * @param mixed $result
     * @param bool  $customizable
     *
     * @return Response
     */
    private function simpleResponseHelper($result, bool $customizable): Response
    {
        $response = (new SimpleResponseDTO($result, $customizable));

        return $this->handleView(View::create($response));
    }
}
