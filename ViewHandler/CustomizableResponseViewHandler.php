<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\ViewHandler;


use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use JMS\Serializer\SerializerInterface;
use KaiGrassnick\SimpleApiBundle\DTO\Response\CustomizableResponseInterface;
use KaiGrassnick\SimpleApiBundle\Serializer\ExclusionStrategy\RemovePropertiesExclusionStrategy;
use KaiGrassnick\SimpleApiBundle\Serializer\ExclusionStrategy\RequestExclusionStrategy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CustomizableResponseViewHandler
 *
 * @package KaiGrassnick\SimpleApiBundle\ViewHandler
 */
class CustomizableResponseViewHandler
{
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;


    /**
     * CustomizableResponseViewHandler constructor.
     *
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    /**
     * @param ViewHandler $handler
     * @param View        $view
     * @param Request     $request
     * @param string      $format
     *
     * @return Response
     */
    public function createResponse(ViewHandler $handler, View $view, Request $request, $format = "json"): Response
    {
        $data = $view->getData();

        if (!$data instanceof CustomizableResponseInterface) {
            return $handler->createResponse($view, $request, $format);
        }

        if (!$data->isCustomizable()) {
            $context = new Context();
            $context->addExclusionStrategy(new RemovePropertiesExclusionStrategy());
            $view->setContext($context);

            return $handler->createResponse($view, $request, $format);
        }

        $normalized = $this->serializer->toArray($data);
        $data->setProperties($this->extractProperties($normalized));
        $view->setData($data);

        $content       = $request->getContent();
        $contentObject = json_decode($content, true);
        $properties    = null;
        if (isset($contentObject["properties"])) {
            $properties = $contentObject["properties"];
        }

        $context = new Context();
        $context->addExclusionStrategy(new RequestExclusionStrategy($properties, $this->getResultProperties()));
        $view->setContext($context);

        return $handler->createResponse($view, $request, $format);
    }


    /**
     * @param array $array
     * @param int   $depth
     *
     * @return array
     */
    private function extractProperties(array $array, int $depth = 0): array
    {
        $properties          = [];
        $referenceKeys       = $this->getReferenceKeys();
        $resultProperties    = $this->getResultProperties();
        $protectedProperties = $this->getProtectedProperties();

        foreach ($array as $key => $value) {
            if (isset($referenceKeys[$key])) {
                continue;
            }

            if ($depth === 0 && isset($protectedProperties[$key])) {
                continue;
            }

            if ($depth != 0 || !isset($resultProperties[$key])) {
                $properties[$key] = new \stdClass();
            }

            if (!is_array($value)) {
                continue;
            }

            if (isset($value[0])) {
                $value = $value[0];
            }

            $extractedProperties = $this->extractProperties($value, $depth + 1);

            if (!isset($properties[$key])) {
                $properties = array_merge($properties, $extractedProperties);
                continue;
            }

            if ($extractedProperties === []) {
                $extractedProperties = new \stdClass();
            }

            $properties[$key] = $extractedProperties;
        }

        return $properties;
    }


    /**
     * @return array|bool[]
     */
    private function getProtectedProperties(): array
    {
        return [
            "properties"  => true,
        ];
    }


    /**
     * @return array|bool[]
     */
    private function getResultProperties(): array
    {
        return [
            "result"           => true,
            "resultSet"        => true,
            "page"             => true,
            "resultCount"      => true,
            "resultsPerPage"   => true,
            "totalPageCount"   => true,
            "totalResultCount" => true,
        ];
    }


    /**
     * @return array|bool[]
     */
    private function getReferenceKeys(): array
    {
        // extract values from abstractViewDto
        return [
            "id"           => true,
            "name"         => true,
            "referenceUrl" => true,
        ];
    }
}
