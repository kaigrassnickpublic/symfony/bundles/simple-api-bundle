<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Paginator;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class DoctrinePaginator
 *
 * @package KaiGrassnick\SimpleApiBundle\Paginator
 */
class DoctrinePaginator
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param int          $page
     * @param int          $resultsPerPage
     *
     * @return Paginator
     */
    public static function getPaginator(QueryBuilder $queryBuilder, int $page, int $resultsPerPage): Paginator
    {
        $firstResultPage = $page - 1;
        $firstResult     = $resultsPerPage * $firstResultPage;

        $query = $queryBuilder->select()->setFirstResult($firstResult)->setMaxResults($resultsPerPage);

        return new Paginator($query);
    }
}
