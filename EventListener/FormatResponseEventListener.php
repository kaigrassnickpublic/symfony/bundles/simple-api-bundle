<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\EventListener;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Class FormatResponseEventListener
 *
 * @package KaiGrassnick\SimpleApiBundle\EventListener
 */
class FormatResponseEventListener
{

    /**
     * @param ResponseEvent $event
     */
    public function onKernelResponse(ResponseEvent $event): void
    {
        $response      = $event->getResponse();
        $contentString = $response->getContent();

        $content = json_decode($contentString, true);

        if ($content === null) {
            return;
        }

        if (isset($content['properties'])) {
            $content['properties'] = $this->propertyArrayToObject($content['properties']);

            if ($content['properties'] === []) {
                $content['properties'] = new \stdClass();
            }
        }

        if (isset($content['type']) && isset($content['title']) && isset($content['status']) && isset($content['detail'])) {
            return;
        }

        $contentArray = [
            "type"   => "https://tools.ietf.org/html/rfc2616#section-10",
            "title"  => Response::$statusTexts[$event->getResponse()->getStatusCode()] ?? 'Undefined',
            "status" => $event->getResponse()->getStatusCode(),
            "detail" => $content,
        ];

        $event->getResponse()->setContent(json_encode($contentArray));

        return;
    }


    /**
     * @param array $properties
     *
     * @return array
     */
    private function propertyArrayToObject(array $properties): array
    {
        foreach ($properties as $key => $value) {
            if ($value === []) {
                $properties[$key] = new \stdClass();
            }

            if (count($value) > 0) {
                $properties[$key] = $this->propertyArrayToObject($value);
            }
        }

        return $properties;
    }
}
