<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2021.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\EventListener;

use JMS\Serializer\Exception\RuntimeException;
use KaiGrassnick\SimpleApiBundle\Exception\InvalidJsonException;
use KaiGrassnick\SimpleApiBundle\Exception\InvalidPropertyTypeException;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Class CustomExceptionEventListener
 *
 * @package KaiGrassnick\SimpleApiBundle\EventListener
 */
class CustomExceptionEventListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        if ($exception instanceof RuntimeException && str_starts_with($exception->getMessage(), "Could not decode JSON")) {
            $event->setThrowable(new InvalidJsonException());

            return;
        }

        if ($exception instanceof \TypeError) {
            $message = $exception->getMessage();
//            preg_match('/\$.+/', $message, $messageParts);
//            preg_match_all('/(?:\$)[^\s]+|\s\K.+/', $messageParts[0], $matches);

//            if (count($matches) !== 1) {
//                return;
//            }

//            $propertyPath = str_replace('$', '', $matches[0][0]);
//            $description  = ucfirst($matches[0][1]);
            $propertyPath = "see original message";
            $description  = "see original message";

            $event->setThrowable(new InvalidPropertyTypeException($propertyPath, $description, $message));

            return;
        }
    }
}
