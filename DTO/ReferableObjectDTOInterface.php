<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO;

/**
 * Interface ReferableObjectDTOInterface
 *
 * @package KaiGrassnick\SimpleApiBundle\DTO
 */
interface ReferableObjectDTOInterface
{
    /**
     * @return string
     */
    public function getId(): string;


    /**
     * @return string
     */
    public function getName(): string;


    /**
     * @return string
     */
    public function getReferenceUrl(): string;
}
