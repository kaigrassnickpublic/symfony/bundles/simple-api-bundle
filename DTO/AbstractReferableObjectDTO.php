<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO;

/**
 * Class AbstractReferableObjectDTO
 *
 * @package KaiGrassnick\SimpleApiBundle\DTO
 */
abstract class AbstractReferableObjectDTO implements ReferableObjectDTOInterface
{
    /**
     * @var string
     */
    private string $id;

    /**
     * @var string
     */
    private string $name;

    /**
     * @var string
     */
    private string $referenceUrl;


    /**
     * AbstractReferableObjectDTO constructor.
     *
     * @param string $id
     * @param string $name
     * @param string $referenceUrl
     */
    public function __construct(string $id, string $name, string $referenceUrl)
    {
        $this->setId($id);
        $this->setName($name);
        $this->setReferenceUrl($referenceUrl);
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }


    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): AbstractReferableObjectDTO
    {
        $this->id = $id;

        return $this;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): AbstractReferableObjectDTO
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return string
     */
    public function getReferenceUrl(): string
    {
        return $this->referenceUrl;
    }


    /**
     * @param string $referenceUrl
     *
     * @return $this
     */
    public function setReferenceUrl(string $referenceUrl): AbstractReferableObjectDTO
    {
        $this->referenceUrl = $referenceUrl;

        return $this;
    }


}
