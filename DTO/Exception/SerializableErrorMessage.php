<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO\Exception;

/**
 * Class SerializableErrorMessage
 *
 * @package KaiGrassnick\SimpleApiBundle\DTO
 */
class SerializableErrorMessage implements \JsonSerializable
{
    /**
     * @var string
     */
    private string $title;

    /**
     * @var mixed
     */
    private $message;


    /**
     * SerializableErrorMessage constructor.
     *
     * @param string $title
     * @param mixed  $message
     */
    public function __construct(string $title, $message)
    {
        $this->title   = $title;
        $this->message = $message;
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }


    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }


    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'title'   => $this->getTitle(),
            'message' => $this->getMessage(),
        ];
    }
}
