<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO\Response;

/**
 * Class PaginatedCollectionResponseDTO
 *
 * @package KaiGrassnick\SimpleApiBundle\DTO\Response
 */
class PaginatedCollectionResponseDTO extends CollectionResponseDTO
{
    /**
     * @var int
     */
    private int $page;

    /**
     * @var int
     */
    private int $resultsPerPage;

    /**
     * @var int
     */
    private int $totalPageCount;

    /**
     * @var int
     */
    private int $totalResultCount;


    /**
     * PaginatedCollectionResponseDTO constructor.
     *
     * @param array $resultSet
     * @param int   $firstResult
     * @param int   $totalResultCount
     * @param int   $resultsPerPage
     * @param bool  $customizable
     */
    public function __construct(array $resultSet, int $firstResult, int $totalResultCount, int $resultsPerPage, bool $customizable)
    {
        $this->page           = intval(floor($firstResult / $resultsPerPage)) + 1;
        $this->resultsPerPage = $resultsPerPage;

        $this->totalPageCount   = intval(ceil($totalResultCount / $resultsPerPage));
        $this->totalResultCount = $totalResultCount;

        parent::__construct($resultSet, $customizable);
    }


    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }


    /**
     * @return int
     */
    public function getResultsPerPage(): int
    {
        return $this->resultsPerPage;
    }


    /**
     * @return int
     */
    public function getTotalPageCount(): int
    {
        return $this->totalPageCount;
    }


    /**
     * @return int
     */
    public function getTotalResultCount(): int
    {
        return $this->totalResultCount;
    }


}
