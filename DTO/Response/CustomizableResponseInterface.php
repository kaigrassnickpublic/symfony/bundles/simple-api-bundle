<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO\Response;

/**
 * Interface CustomizableResponseInterface
 *
 * @package KaiGrassnick\SimpleApiBundle\DTO\Response
 */
interface CustomizableResponseInterface
{
    /**
     * @return bool
     */
    public function isCustomizable(): bool;


    /**
     * @return array
     */
    public function getProperties(): array;


    /**
     * @param array $properties
     *
     * @return CustomizableResponseInterface
     */
    public function setProperties(array $properties): CustomizableResponseInterface;
}
