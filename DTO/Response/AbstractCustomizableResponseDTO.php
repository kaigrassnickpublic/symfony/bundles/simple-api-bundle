<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO\Response;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class AbstractCustomizableResponseDTO
 *
 * @package KaiGrassnick\SimpleApiBundle\DTO\Response
 */
abstract class AbstractCustomizableResponseDTO implements CustomizableResponseInterface
{
    /**
     * @var bool
     *
     * @Serializer\Exclude()
     */
    private bool $customizable;

    /**
     * @var array
     */
    private array $properties;


    /**
     * AbstractCustomizableResponseDTO constructor.
     *
     * @param bool $customizable
     */
    public function __construct(bool $customizable = true)
    {
        $this->customizable = $customizable;
        $this->properties   = [];
    }


    /**
     * @return bool
     */
    public function isCustomizable(): bool
    {
        return $this->customizable;
    }


    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->properties;
    }


    /**
     * @param array $properties
     *
     * @return AbstractCustomizableResponseDTO
     */
    public function setProperties(array $properties): AbstractCustomizableResponseDTO
    {
        $this->properties = $properties;

        return $this;
    }


}
