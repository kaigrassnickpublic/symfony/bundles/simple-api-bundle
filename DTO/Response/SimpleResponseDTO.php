<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO\Response;

/**
 * Class SimpleResponseDTO
 *
 * @package KaiGrassnick\SimpleApiBundle\DTO\Response
 */
class SimpleResponseDTO extends AbstractCustomizableResponseDTO
{
    /**
     * @var mixed
     */
    private $result;


    /**
     * SimpleResponseDTO constructor.
     *
     * @param mixed $result
     * @param bool  $customizable
     */
    public function __construct($result, bool $customizable)
    {
        $this->setResult($result);

        parent::__construct($customizable);
    }


    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }


    /**
     * @param mixed $result
     *
     * @return SimpleResponseDTO
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

}
