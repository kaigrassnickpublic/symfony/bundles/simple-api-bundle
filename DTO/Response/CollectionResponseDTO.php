<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO\Response;

/**
 * Class CollectionResponseDTO
 *
 * @package KaiGrassnick\SimpleApiBundle\DTO\Response
 */
class CollectionResponseDTO extends AbstractCustomizableResponseDTO
{
    /**
     * @var array
     */
    private array $resultSet;

    /**
     * @var int
     */
    private int $resultCount;


    /**
     * CollectionResponseDTO constructor.
     *
     * @param array $resultSet
     * @param bool  $customizable
     */
    public function __construct(array $resultSet, bool $customizable)
    {
        $this->resultSet   = $resultSet;
        $this->resultCount = count($resultSet);

        parent::__construct($customizable);
    }


    /**
     * @return array
     */
    public function getResultSet(): array
    {
        return $this->resultSet;
    }


    /**
     * @return int
     */
    public function getResultCount(): int
    {
        return $this->resultCount;
    }

}
