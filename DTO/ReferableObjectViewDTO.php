<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO;

/**
 * Class ReferableObjectViewDTO
 *
 * @package App\Package\Api\DTO
 */
class ReferableObjectViewDTO extends AbstractReferableObjectDTO
{
}
