<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DTO;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class PaginatedItemsDTO
 *
 * @package KaiGrassnick\SimpleApiBundle\DTO
 */
class PaginatedItemsDTO
{
    /**
     * @var Paginator
     */
    private Paginator $paginator;

    /**
     * @var array
     */
    private array $items;


    /**
     * PaginatedItemsDTO constructor.
     *
     * @param array     $items
     * @param Paginator $paginator
     */
    public function __construct(array $items, Paginator $paginator)
    {
        $this->items     = $items;
        $this->paginator = $paginator;
    }


    /**
     * @return Paginator
     */
    public function getPaginator(): Paginator
    {
        return $this->paginator;
    }


    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }


}
