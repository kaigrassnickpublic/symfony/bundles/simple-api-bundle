<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Serializer\ExclusionStrategy;

use JMS\Serializer\Context;
use JMS\Serializer\Exclusion\ExclusionStrategyInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;
use KaiGrassnick\SimpleApiBundle\DTO\AbstractReferableObjectDTO;
use KaiGrassnick\SimpleApiBundle\DTO\Response\AbstractCustomizableResponseDTO;
use KaiGrassnick\SimpleApiBundle\DTO\Response\CollectionResponseDTO;
use KaiGrassnick\SimpleApiBundle\DTO\Response\PaginatedCollectionResponseDTO;
use KaiGrassnick\SimpleApiBundle\DTO\Response\SimpleResponseDTO;

/**
 * Class RequestExclusionStrategy
 *
 * @package KaiGrassnick\SimpleApiBundle\Serializer\ExclusionStrategy
 */
final class RequestExclusionStrategy implements ExclusionStrategyInterface
{

    /**
     * @var array | null
     */
    private ?array $properties;

    /**
     * @var array
     */
    private array $basePaths;


    /**
     * RequestExclusionStrategy constructor.
     *
     * @param array|null $properties
     * @param array      $basePaths
     */
    public function __construct(?array $properties, array $basePaths = [])
    {
        $this->properties = $properties;
        $this->basePaths  = $basePaths;
    }


    /**
     * {@inheritDoc}
     */
    public function shouldSkipClass(ClassMetadata $metadata, Context $context): bool
    {
        return false;
    }


    /**
     * {@inheritDoc}
     */
    public function shouldSkipProperty(PropertyMetadata $property, Context $context): bool
    {
        $shouldKeepClasses = [
            CollectionResponseDTO::class,
            SimpleResponseDTO::class,
            AbstractCustomizableResponseDTO::class,
            AbstractReferableObjectDTO::class,
            PaginatedCollectionResponseDTO::class,
        ];

        if (in_array($property->class, $shouldKeepClasses)) {
            return false;
        }

        if($this->properties === null) {
            return false;
        }

        if (count($this->properties) === 0) {
            return true;
        }

        return $this->isNotRequested($property->name, $context->getCurrentPath());
    }


    /**
     * @param string $propertyName
     * @param array  $currentPath
     *
     * @return bool
     */
    private function isNotRequested(string $propertyName, array $currentPath): bool
    {
        $properties = $this->properties;

        foreach ($currentPath as $path) {
            if (isset($this->basePaths[$path])) {
                continue;
            }

            if (!isset($properties[$path])) {
                return true;
            }

            $properties = $properties[$path];
        }

        if (!isset($properties[$propertyName])) {
            return true;
        }

        return false;
    }
}
