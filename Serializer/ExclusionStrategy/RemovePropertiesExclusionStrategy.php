<?php declare(strict_types=1);

/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Serializer\ExclusionStrategy;

use JMS\Serializer\Context;
use JMS\Serializer\Exclusion\ExclusionStrategyInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;

/**
 * Class RemovePropertiesExclusionStrategy
 *
 * @package KaiGrassnick\SimpleApiBundle\Serializer\ExclusionStrategy
 */
final class RemovePropertiesExclusionStrategy implements ExclusionStrategyInterface
{
    /**
     * {@inheritDoc}
     */
    public function shouldSkipClass(ClassMetadata $metadata, Context $context): bool
    {
        return false;
    }


    /**
     * {@inheritDoc}
     */
    public function shouldSkipProperty(PropertyMetadata $property, Context $context): bool
    {
        return $context->getCurrentPath() === [] && $property->name === "properties";
    }
}
