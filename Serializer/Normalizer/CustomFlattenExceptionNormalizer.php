<?php declare(strict_types=1);

/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Serializer\Normalizer;

use FOS\RestBundle\Util\ExceptionValueMap;
use KaiGrassnick\SimpleApiBundle\Exception\SerializedMessageExceptionInterface;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CustomFlattenExceptionNormalizer implements NormalizerInterface
{
    /**
     * @var ExceptionValueMap
     */
    private ExceptionValueMap $messagesMap;

    /**
     * @var bool
     */
    private bool $debug;


    /**
     * CustomFlattenExceptionNormalizer constructor.
     *
     * @param ExceptionValueMap $messagesMap
     * @param bool              $debug
     */
    public function __construct(ExceptionValueMap $messagesMap, bool $debug)
    {
        $this->messagesMap = $messagesMap;
        $this->debug       = $debug;
    }


    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        if (isset($context['status_code'])) {
            $statusCode = $context['status_code'];
        } else {
            $statusCode = $object->getStatusCode();
        }

        $showMessage = $this->messagesMap->resolveFromClassName($object->getClass());

        if ($showMessage) {
            $message = $object->getMessage();
        } else {
            $message = Response::$statusTexts[$statusCode] ?? 'error';
        }

        if (strpos($message, SerializedMessageExceptionInterface::JSON_MESSAGE_PREFIX) !== false) {
            $message       = str_replace(SerializedMessageExceptionInterface::JSON_MESSAGE_PREFIX, "", $message);
            $messageObject = json_decode($message);

            $context['title'] = $messageObject->title;
            $message          = $messageObject->message;
        }

        $object->setHeaders($object->getHeaders() + ['Content-Type' => 'application/problem+json']);

        return [
            'type'   => $context['type'] ?? 'https://tools.ietf.org/html/rfc2616#section-10',
            'title'  => $context['title'] ?? 'An error occurred',
            'status' => $statusCode,
            'detail' => $message,
        ];
    }


    /**
     * @param mixed       $data
     * @param string|null $format
     *
     * @return bool
     */
    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof FlattenException;
    }
}
