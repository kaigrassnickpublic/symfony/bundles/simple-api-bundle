<?php declare(strict_types=1);

/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Serializer;

use JMS\Serializer\Construction\ObjectConstructorInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Visitor\DeserializationVisitorInterface;

/**
 * Class ObjectConstructor
 *
 * @package KaiGrassnick\SimpleApiBundle\Serializer
 */
class ObjectConstructor implements ObjectConstructorInterface
{
    /**
     * @param DeserializationVisitorInterface $visitor
     * @param ClassMetadata                   $metadata
     * @param mixed                           $data
     * @param array                           $type
     * @param DeserializationContext          $context
     *
     * @return object|null
     */
    public function construct(DeserializationVisitorInterface $visitor, ClassMetadata $metadata, $data, array $type, DeserializationContext $context): ?object
    {
        $className = $metadata->name;

        return new $className();
    }
}
