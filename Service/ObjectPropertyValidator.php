<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Service;


use KaiGrassnick\SimpleApiBundle\Exception\InvalidPropertyException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ObjectPropertyValidator
 *
 * @package KaiGrassnick\SimpleApiBundle\Service
 */
class ObjectPropertyValidator
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;


    /**
     * ObjectPropertyValidator constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }


    /**
     * @param object $input
     *
     * @throws \Exception
     * @throws InvalidPropertyException
     */
    public function validateObject(object $input): void
    {
        $valid = $this->validator->validate($input);
        if ($valid->count() > 0) {
            throw new InvalidPropertyException($valid);
        }
    }
}
