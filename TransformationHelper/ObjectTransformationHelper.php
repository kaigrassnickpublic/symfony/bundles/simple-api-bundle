<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\TransformationHelper;

use KaiGrassnick\SimpleApiBundle\Exception\InvalidJsonException;
use KaiGrassnick\SimpleApiBundle\Exception\UnknownPropertyException;
use ReflectionException;

/**
 * Class ObjectTransformationHelper
 *
 * @package KaiGrassnick\SimpleApiBundle\TransformationHelper
 */
class ObjectTransformationHelper
{
    /**
     * @param string $content
     *
     * @return array
     */
    public function getPropertiesFromJsonContent(string $content): array
    {
        $requestArray = json_decode($content, true);

        if ($requestArray === null) {
            throw new InvalidJsonException();
        }

        return $requestArray;
    }


    /**
     * @param iterable $requestArray
     * @param object   $object
     *
     * @throws ReflectionException
     * @return object
     */
    public function updateObject(iterable $requestArray, object $object): object
    {
        $availableProperties = $this->getAvailableProperties($object);

        foreach ($requestArray as $requestProperty => $value) {
            $this->validateRequestedProperty($requestProperty, $availableProperties);

            $setter = $availableProperties[$requestProperty];
            $object->$setter($value);
        }

        return $object;
    }


    /**
     * @param string $property
     * @param array  $availableProperties
     */
    private function validateRequestedProperty(string $property, array $availableProperties): void
    {
        if (!isset($availableProperties[$property])) {
            throw new UnknownPropertyException($property, $availableProperties);
        }
    }


    /**
     * @param object $dto
     *
     * @throws ReflectionException
     * @return array
     */
    private function getAvailableProperties(object $dto): array
    {
        $reflectionObject = new \ReflectionClass($dto);
        $dtoMethods       = $reflectionObject->getMethods(\ReflectionProperty::IS_PUBLIC);

        $prefix       = "set";
        $prefixLength = strlen($prefix);

        $availableMethods = [];
        foreach ($dtoMethods as $dtoMethod) {
            $methodName = $dtoMethod->getName();

            // ignore methods not starting with set
            if (substr($methodName, 0, $prefixLength) !== $prefix) {
                continue;
            }

            // ignore methods which are false positives (eg. settingChange) first char after 'set' needs to be uppercase
            $firstChar = substr($methodName, $prefixLength, 1);
            if ($firstChar !== strtoupper($firstChar)) {
                continue;
            }

            // ignore methods which require parameters
            if ($dtoMethod->getNumberOfParameters() !== 1) {
                continue;
            }

            $propertyName                    = lcfirst(substr($methodName, $prefixLength));
            $availableMethods[$propertyName] = $methodName;
        }

        return $availableMethods;
    }
}
