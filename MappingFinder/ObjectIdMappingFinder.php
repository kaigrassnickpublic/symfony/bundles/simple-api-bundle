<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\MappingFinder;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ObjectIdMappingFinder
 *
 * @package KaiGrassnick\SimpleApiBundle\MappingFinder
 */
class ObjectIdMappingFinder
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param string                 $className
     * @param string                 $id
     *
     * @return object|null
     */
    public static function findObjectWithId(EntityManagerInterface $entityManager, string $className, string $id): ?object
    {
        $alias = str_replace("\\", "_", $className);

        $qb   = $entityManager->getRepository($className)->createQueryBuilder($alias);
        $expr = $qb->expr();

        $query = $qb->select($alias)->where($expr->eq($alias . '.id', ':id'));
        $query->setParameter('id', $id);

        $result = $query->getQuery()->execute();

        if (count($result) > 0) {
            return $result[0];
        }

        return null;
    }


    /**
     * @param EntityManagerInterface $entityManager
     * @param string                 $className
     * @param array                  $ids
     *
     * @return array
     */
    public static function findObjectsWithIds(EntityManagerInterface $entityManager, string $className, array $ids): array
    {
        $alias = str_replace("\\", "_", $className);

        /** @var QueryBuilder $qb */
        $qb   = $entityManager->getRepository($className)->createQueryBuilder($alias);
        $expr = $qb->expr();

        $query = $qb->select($alias)->where($expr->in($alias . '.id', ':ids'));
        $query->setParameter('ids', array_values($ids));

        return $query->getQuery()->execute();
    }
}
