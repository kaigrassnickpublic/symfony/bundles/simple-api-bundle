<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Exception;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class UnknownPropertyException
 *
 * @package KaiGrassnick\SimpleApiBundle\Exception
 */
class UnknownPropertyException extends BadRequestHttpException
{
    /**
     * UnknownPropertyException constructor.
     *
     * @param string $invalidProperty
     * @param array  $availableProperties
     */
    public function __construct(string $invalidProperty, array $availableProperties)
    {
        $message = sprintf("Unknown property: %s. Allowed properties: %s", $invalidProperty, implode(", ", array_keys($availableProperties)));

        parent::__construct($message);
    }
}
