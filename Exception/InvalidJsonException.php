<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Exception;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class InvalidJsonException
 *
 * @package KaiGrassnick\SimpleApiBundle\Exception
 */
class InvalidJsonException extends BadRequestHttpException
{
    /**
     * InvalidJsonException constructor.
     */
    public function __construct()
    {
        parent::__construct("Invalid JSON");
    }
}
