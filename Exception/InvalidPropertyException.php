<?php declare(strict_types=1);

/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Exception;

use KaiGrassnick\SimpleApiBundle\DTO\Exception\SerializableErrorMessage;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class InvalidPropertyException
 *
 * @package KaiGrassnick\SimpleApiBundle\Exception
 */
class InvalidPropertyException extends BadRequestHttpException implements SerializedMessageExceptionInterface
{

    /**
     * InvalidPropertyException constructor.
     *
     * @param ConstraintViolationListInterface $constraintViolationList
     */
    public function __construct(ConstraintViolationListInterface $constraintViolationList)
    {
        parent::__construct($this->createErrorMessageFromConstraintViolationList($constraintViolationList));
    }


    /**
     * @param ConstraintViolationListInterface $constraintViolationList
     *
     * @return string
     */
    private function createErrorMessageFromConstraintViolationList(ConstraintViolationListInterface $constraintViolationList): string
    {
        $violations = [];
        /** @var ConstraintViolation $constraintViolation */
        foreach ($constraintViolationList as $constraintViolation) {

            $violations[] = [
                'propertyPath' => $constraintViolation->getPropertyPath(),
                'message'      => $constraintViolation->getMessage(),
                'errorName'    => $constraintViolation->getConstraint()->getErrorName($constraintViolation->getCode()),
                'code'         => $constraintViolation->getCode(),
            ];
        }

        $message = new SerializableErrorMessage("Validation failed", ["errors" => $violations]);

        return self::JSON_MESSAGE_PREFIX . json_encode($message);
    }
}
