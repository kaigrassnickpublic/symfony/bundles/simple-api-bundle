<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Exception;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class SpeakingBadRequestException
 *
 * @package KaiGrassnick\SimpleApiBundle\Exception
 */
class SpeakingBadRequestException extends BadRequestHttpException
{
}
