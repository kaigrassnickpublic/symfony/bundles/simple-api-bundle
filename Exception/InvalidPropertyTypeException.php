<?php declare(strict_types=1);

/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Exception;

use KaiGrassnick\SimpleApiBundle\DTO\Exception\SerializableErrorMessage;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class InvalidPropertyException
 *
 * @package KaiGrassnick\SimpleApiBundle\Exception
 */
class InvalidPropertyTypeException extends BadRequestHttpException implements SerializedMessageExceptionInterface
{
    /**
     * InvalidPropertyTypeException constructor.
     *
     * @param string $propertyPath
     * @param string $description
     * @param string $originalMessage
     */
    public function __construct(string $propertyPath, string $description, string $originalMessage)
    {
        parent::__construct($this->createErrorMessage($propertyPath, $description, $originalMessage));
    }


    /**
     * @param string $propertyPath
     * @param string $description
     * @param string $originalMessage
     *
     * @return string
     */
    private function createErrorMessage(string $propertyPath, string $description, string $originalMessage): string
    {
        $violations[] = [
            'propertyPath'    => $propertyPath,
            'message'         => sprintf("Invalid property type. %s", $description),
            'originalMessage' => $originalMessage,
            'errorName'       => "INVALID_TYPE_ERROR",
            'code'            => "78620ac1-983b-46ef-abca-46afb78a7a2b",
        ];

        $message = new SerializableErrorMessage("Validation failed", ["errors" => $violations]);

        return self::JSON_MESSAGE_PREFIX . json_encode($message);
    }
}
