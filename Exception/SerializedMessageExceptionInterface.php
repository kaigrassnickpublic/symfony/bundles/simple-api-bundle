<?php declare(strict_types=1);

/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Exception;

/**
 * Interface SerializedMessageExceptionInterface
 *
 * @package KaiGrassnick\SimpleApiBundle\Exception
 */
interface SerializedMessageExceptionInterface
{
    public const JSON_MESSAGE_PREFIX = "SER_MSG_PREFIX#";
}
