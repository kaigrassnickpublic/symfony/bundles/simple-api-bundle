<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\Exception;


use Throwable;

/**
 * Class ViewGeneratorLimitException
 *
 * @package KaiGrassnick\SimpleApiBundle\Exception
 */
class ViewGeneratorLimitException extends \Exception
{
    /**
     * ViewGeneratorLimitException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "View Generator ran into an endless loop. Aborted due to limit", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
