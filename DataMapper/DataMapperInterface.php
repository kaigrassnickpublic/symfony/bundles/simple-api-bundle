<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DataMapper;


use KaiGrassnick\SimpleApiBundle\DTO\ReferableObjectDTOInterface;

/**
 * Interface DataMapperInterface
 *
 * @package KaiGrassnick\SimpleApiBundle\DataMapper
 */
interface DataMapperInterface
{
    /**
     * @param object $sourceObject
     * @param array  $refOnlyClass
     * @param int    $depth
     *
     * @return ReferableObjectDTOInterface
     */
    public function transformObjectToDTO(object $sourceObject, array $refOnlyClass = [], int $depth = 0): ReferableObjectDTOInterface;
}
