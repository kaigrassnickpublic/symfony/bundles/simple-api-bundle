<?php declare(strict_types=1);
/*******************************************************************************
 * Copyright (c) 2020.
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleApiBundle\DataMapper;


use Doctrine\Common\Collections\Collection;
use KaiGrassnick\NameConverterBundle\NameConverter\CamelCaseToSnakeCaseNameConverter;
use KaiGrassnick\SimpleApiBundle\DTO\ReferableObjectDTOInterface;
use KaiGrassnick\SimpleApiBundle\DTO\ReferableObjectViewDTO;
use KaiGrassnick\SimpleApiBundle\Exception\ViewGeneratorLimitException;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class AbstractDataMapper
 *
 * @package KaiGrassnick\SimpleApiBundle\DataMapper
 */
abstract class AbstractDataMapper implements DataMapperInterface
{
    public const HIDDEN_VALUE = "**********";

    /**
     * @var UrlGeneratorInterface
     */
    private UrlGeneratorInterface $urlGenerator;


    /**
     * AbstractDataMapper constructor.
     *
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }


    /**
     * @param Collection $referenceCollection
     * @param string     $dataMapperClass
     * @param array      $refOnlyClass
     * @param int        $depth
     *
     * @throws ViewGeneratorLimitException
     * @return array|ReferableObjectDTOInterface[]
     */
    protected function getReferenceDTOArray(Collection $referenceCollection, string $dataMapperClass, array $refOnlyClass, int $depth): array
    {
        $referenceArray = $referenceCollection->toArray();
        $referenceDTOs  = [];

        foreach ($referenceArray as $referenceObject) {
            $referenceDTOs[] = $this->getReferenceDTO($referenceObject, $dataMapperClass, $refOnlyClass, $depth);
        }

        return $referenceDTOs;
    }


    /**
     * @param Collection $referenceCollection
     *
     * @return array
     */
    protected function getReferenceIdArray(Collection $referenceCollection): array
    {
        $referenceArray = $referenceCollection->toArray();
        $referenceIds   = [];

        foreach ($referenceArray as $referenceObject) {
            $referenceIds[] = $referenceObject->getId();
        }

        return $referenceIds;
    }


    /**
     * @param object $referenceObject
     * @param string $dataMapperClass
     * @param array  $refOnlyClass
     * @param int    $depth
     *
     * @throws ViewGeneratorLimitException
     * @return ReferableObjectDTOInterface
     */
    protected function getReferenceDTO(object $referenceObject, string $dataMapperClass, array $refOnlyClass, int $depth): ReferableObjectDTOInterface
    {
        $refOnlyClass[get_class($this)] = true;

        if ($depth > 10) {
            throw new ViewGeneratorLimitException();
        }

        $depth++;

        if (isset($refOnlyClass[$dataMapperClass])) {
            return new ReferableObjectViewDTO($referenceObject->getId(), $referenceObject->getName(), $this->getReferenceUrl($referenceObject));
        }

        /** @var DataMapperInterface $dataMapper */
        $dataMapper = (new $dataMapperClass($this->urlGenerator));

        return $dataMapper->transformObjectToDTO($referenceObject, $refOnlyClass, $depth);
    }


    /**
     * @param object $referenceObject
     * @param string $route
     *
     * @return string
     */
    protected function getReferenceUrl(object $referenceObject, string $route = ""): string
    {
        if ($route === "") {
            $route = $this->getRouteForClass($referenceObject);
        }

        if ($route === "") {
            throw new RouteNotFoundException("Route name empty. Route name not given and could not be generated automatically.");
        }

        return $this->urlGenerator->generate($route, ["id" => $referenceObject->getId()]);
    }


    /**
     * @return string
     */
    abstract protected function getRoutePrefix(): string;


    /**
     * @param object $referenceObject
     *
     * @return string
     */
    private function getRouteForClass(object $referenceObject): string
    {
        $route = "";

        try {
            $reflection  = new ReflectionClass($referenceObject);
            $className   = $reflection->getShortName();
            $pathSnippet = (new CamelCaseToSnakeCaseNameConverter())->normalize($className);
            $route       = sprintf("%s_%s_view", $this->getRoutePrefix(), $pathSnippet);
        } catch (ReflectionException $exception) {
            // do nothing here
        }

        return $route;
    }
}
